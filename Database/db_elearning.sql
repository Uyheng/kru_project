-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2017 at 11:44 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `addingcourse`
--

CREATE TABLE `addingcourse` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_description` text,
  `price` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addingcourse`
--

INSERT INTO `addingcourse` (`id`, `title`, `title_description`, `price`) VALUES
(7, 'IOS', 'Ios is love', 100),
(8, 'Android', 'Android is love', 100),
(9, 'Java', 'Java is love', 100),
(10, 'Web ', 'Web is love', 100);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('ci3uumgg1kfb1s631t7o9hrd36lrpd2r', '::1', 1506764739, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363736343732323b61646d696e5f6c6f67696e7c733a313a2231223b),
('i5s77979uv7bpriubvrs8j803vlikpkq', '::1', 1506765069, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363736343933303b61646d696e5f6c6f67696e7c733a313a2231223b),
('4bnrbiptseekjs1jlkj06151qpf7grvn', '::1', 1506765285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363736353234383b61646d696e5f6c6f67696e7c733a313a2231223b),
('dre1g3ainl59fbhpnvs3s576cqv05jof', '::1', 1506765763, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363736353736333b),
('2tfs3gm1dkrur7qcpoc9v677a30d342t', '::1', 1506766142, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363736363134323b),
('kpqjp5q3764s5r9nr3h54h6lq1pig4b8', '::1', 1506835588, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530363833353333343b61646d696e5f6c6f67696e7c733a313a2231223b),
('6lej7tlfg2hpfu8jg14a57b3ftes3p61', '::1', 1507369703, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373336393730333b61646d696e5f6c6f67696e7c733a313a2231223b),
('npu033jqqkglr4d2nqtb3vstsipalarn', '::1', 1507370094, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337303039343b61646d696e5f6c6f67696e7c733a313a2231223b),
('5279g3l4tbn6uek45gp3utk0hhogbj8l', '::1', 1507370520, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337303532303b61646d696e5f6c6f67696e7c733a313a2231223b),
('tqt68861tsemggp6sffqsb06hmdpagua', '::1', 1507370927, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337303932373b61646d696e5f6c6f67696e7c733a313a2231223b),
('ccghgc2rui2sm6cajd62is3ob2q0t3aj', '::1', 1507372824, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337323832343b61646d696e5f6c6f67696e7c733a313a2231223b),
('q6dr8coid28ji05o40aoffgq9p6k4adf', '::1', 1507373156, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337333135363b61646d696e5f6c6f67696e7c733a313a2231223b),
('6i68ha8qnocop7r6fi3a2f8dj1vshohf', '::1', 1507373556, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337333535363b61646d696e5f6c6f67696e7c733a313a2231223b),
('j3qmuvk6e0bn1ofkd90nk07njjh7d3ss', '::1', 1507373962, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337333936323b61646d696e5f6c6f67696e7c733a313a2231223b),
('d9pb4ppmgp9oo9or60kp90caupa8o64k', '::1', 1507374459, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337343435393b61646d696e5f6c6f67696e7c733a313a2231223b),
('96ghs4nkqfq054rbjtnop5amoo8vki48', '::1', 1507374960, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337343936303b61646d696e5f6c6f67696e7c733a313a2231223b),
('resq1u07989j7q52oob8bdo15fut7n9s', '::1', 1507375295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337353239353b61646d696e5f6c6f67696e7c733a313a2231223b),
('63gjjp9r2qqeuedgocuhqo3vm7iv0ago', '::1', 1507375630, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337353633303b61646d696e5f6c6f67696e7c733a313a2231223b),
('ri5n54so6m4nf85ei5bau4b6tqfa44ep', '::1', 1507375932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337353933323b61646d696e5f6c6f67696e7c733a313a2231223b),
('629qj23a4v8nl4bjlp2rvbhatqoicrs8', '::1', 1507376363, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337363336333b61646d696e5f6c6f67696e7c733a313a2231223b),
('5en4ntmi0mqle6bhdqqb3shn0bdda0dl', '::1', 1507376553, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373337363336333b61646d696e5f6c6f67696e7c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_description` text,
  `course_price` double DEFAULT NULL,
  `study_date` varchar(255) DEFAULT NULL,
  `study_time` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `title`, `title_description`, `course_price`, `study_date`, `study_time`, `start_date`, `duration`, `content`) VALUES
(15, '10', NULL, 100, 'Mon-Fri', '14:00-17:00', '2017-09-30', '45', 'DSAFASDFDSAF'),
(16, '9', NULL, 100, 'Saturday', '8:00-11:00, 2:00-5:00', '2017-09-30', '50', 'dsafdsafsdafsadfSDAf<div>sadf</div><div>sda</div><div>f</div><div>sad</div><div>f</div><div>sda</div><div>f</div><div>asdf</div>');

-- --------------------------------------------------------

--
-- Table structure for table `db_job`
--

CREATE TABLE `db_job` (
  `id` int(11) NOT NULL,
  `jobtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `companyname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingdate` date NOT NULL,
  `titledescription` text COLLATE utf8_unicode_ci,
  `created` date DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `db_job`
--

INSERT INTO `db_job` (`id`, `jobtitle`, `companyname`, `location`, `closingdate`, `titledescription`, `created`, `content`) VALUES
(16, 'IT Programmer', 'Manulife(Cambodia) PLC', 'Phnom Penh', '2017-09-01', 'Senior Specialist', NULL, '<h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Job Description</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">** Key Responsibilities:<br><br>• Responsible for assisting the implementation of various projects for application systems<br>• Participate in analysis, design, coding, testing and implementation throughout the whole development cycle<br>• Work with local and regional team in delivering the projects<br>• Perform other related tasks as requested<br><br></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Job Requirements</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">** Qualification:<br><br>• University degree in any computer related discipline is preferred<br>• 1- 3 years relevant working experience with solid experience in .Net and Database server (Oracle is an advantage)&nbsp;<br>• Sound knowledge of system development life cycle (SDLC) standard<br>• Sound knowledge of system development with .NET<br>• Prior working experience in life insurance or financial industry is an advantage<br>• Computer literate in MS Word, Excel and PowerPoint<br>• Proven analysis and problem solving skills<br>• Strong communication and interpersonal skills&nbsp;<br>• Able to work effectively in a fast paced and demanding environment and under pressure<br>• High sense of quality and strong personal drive<br>• Good command of spoken and written English<br>• Fast learner with high commitment<br><br>Interested applicants meeting the above requirements should submit their CVs, covering letters<br>and National ID card by clicking on Click here for current opportunities. For more details please<br>contact Ms. Kimleng Moul via kimleng_moul@manulife.com. Or Ms. Choury Chim via<br>choury_chim@manulife.com. Application should be submitted before<br>before 05 September 2017. Only shortlisted candidates will be contacted<br><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; background-color: rgb(255, 255, 255);"><tbody><tr><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;"><p style="padding: 0px; margin-bottom: 0px;"><a href="https://manulife.taleo.net/careersection/external_global/moresearch.ftl?lang=en&amp;location=194940181072" style="color: rgb(63, 84, 137);"><b><font size="3">Click here for detail :&nbsp;</font></b><br><br><br></a></p></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">How To Apply</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; background-color: rgb(255, 255, 255);"><tbody><tr><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 25px;"><p style="padding: 0px; margin-bottom: 0px;">1. Please feel free to&nbsp;<a href="http://www.camhr.com/pages/user/register.jsp" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">register</span></a>&nbsp;with us to get a great job opportunity and achieve your dream</p><p style="padding: 0px; margin-bottom: 0px;">2. If you want to apply a job by one click with “<span style="color: rgb(255, 0, 0);">Apply Now</span>” button, please&nbsp;<a href="http://www.camhr.com/pages/user/cv/index.action" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">create a CV</span></a>&nbsp;first and employers will review your CV online.</p><p style="padding: 0px; margin-bottom: 0px;">Click&nbsp;<a href="http://www.camhr.com/pages/articles/articleList.jsp?articleType=240" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">here</span>&nbsp;</a>to learn how to register and post a CV online!</p></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Contact Information</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 128px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Contact Person</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">Ms. Kimleng Moul</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Phone</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">023 965 965</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Email</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">kimleng_moul@manulife.com.</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Website</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;"><a target="_blank" href="http://www.manulife.com.kh/" style="color: rgb(63, 84, 137);">http://www.manulife.com.kh</a></td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Address</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">104 Russian Federation Blvd | Phnom Penh, Phnom Penh</td></tr></tbody></table><br></td></tr></tbody></table>'),
(17, 'PHP developer in Battambang', 'VTech. Software Solution', 'BTB', '2017-08-31', 'PHP Only', NULL, '<h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;"><br></h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;"></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Job Requirements</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">- Bachelor’s degree in the Computer Science or related fields<br><br>- At least 3 years’ experience in Web Programming<br><br>- Knowledge of Web Programming / Database.<br><br>- Knowledge of PHP, C#, SQL Server and Web Server&nbsp;<br><br>- Knowledge of computer networking (Microsoft windows server, Linux, firewall…)<br><br>- Professional and ethnic behaviours<br><br>- Strong analytical and communication skills<br><br>- Enthusiastic and an interest in all things Computer<br><br>- Able to work as part of a team<br><br>- Good knowledge of English in reading, speaking and writing<br><br>Thanks</td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">How To Apply</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 25px;"><p style="padding: 0px; margin-bottom: 0px;">1. Please feel free to&nbsp;<a href="http://www.camhr.com/pages/user/register.jsp" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">register</span></a>&nbsp;with us to get a great job opportunity and achieve your dream</p><p style="padding: 0px; margin-bottom: 0px;">2. If you want to apply a job by one click with “<span style="color: rgb(255, 0, 0);">Apply Now</span>” button, please&nbsp;<a href="http://www.camhr.com/pages/user/cv/index.action" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">create a CV</span></a>&nbsp;first and employers will review your CV online.</p><p style="padding: 0px; margin-bottom: 0px;">Click&nbsp;<a href="http://www.camhr.com/pages/articles/articleList.jsp?articleType=240" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">here</span>&nbsp;</a>to learn how to register and post a CV online!</p></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Contact Information</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 128px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Contact Person</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">Vannavy Vicheanak</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Phone</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">096 926 9999</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Email</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">info@vtechss.com</td></tr></tbody></table>'),
(18, 'Java Developer ', 'Fuxin Steel Buildings', 'Phnom Penh', '2017-09-05', 'Salary 400$ Up', NULL, '<h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Job Description</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">•Main Responsibilities:<br>- Assisting in the design, implementation, and ongoing maintenance of the Global/Local AX as ERP in Light Gauge Steel Building System.<br>-	Work in collaborative building system teams to iteratively construct enterprise software solutions&nbsp;<br>-	Provide architectural recommendations to efficiency build dynamics AX solutions for light gauge steel building system and others specified system<br>- Responsible for all activities leading to the successful implementation of medium to large-size projects sale through their entire life cycle or for technical support of major functional areas<br>-	Work with internal customers and external consultants and vendors to develop functional business system design and solutions<br>-	Coordinate problem resolution in support of the business and system software<br>- Collaborate with user on the best practices around Dynamics AX programming practices<br>-	Other tasks as per assigned by superior.<br><br><br></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Job Requirements</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 20px; vertical-align: top;">&nbsp;</th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">•	25-30 years old<br>•	Bachelor Of Information of Technology or equivalent<br>•	1+ years of experience customizing Microsoft Dynamic AX and general. NET or X++ development experience<br>•	Knowledge in MS Dynamic AX, MS office: word, excel, Power point<br>•	Good command in English<br>•	Written, verbal, Analytical and interpersonal skills is a plus<br><br><br><br>BENIFITS:<br>• Bonus<br>• Yearly Salary Increment<br>• Insurance&nbsp;<br>• Others<br><br>HOW TO APPLY:<br>• Contact Person:Linda Soy (Ms.)/Lun Seila ( Mr.)<br>• Telephone:070-824-247 / 097-806-9464/093-300-414<br>• Email:recruitment@isisteel.com.kh<br><br>ADDRESS:F14, KMH Industrial Park, Phum Trapang Thleung, Sangkat Chom Chao, Khan Porsenchey,<br>Phnom Penh, Cambodia<br><br></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">How To Apply</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 25px;"><p style="padding: 0px; margin-bottom: 0px;">1. Please feel free to&nbsp;<a href="http://www.camhr.com/pages/user/register.jsp" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">register</span></a>&nbsp;with us to get a great job opportunity and achieve your dream</p><p style="padding: 0px; margin-bottom: 0px;">2. If you want to apply a job by one click with “<span style="color: rgb(255, 0, 0);">Apply Now</span>” button, please&nbsp;<a href="http://www.camhr.com/pages/user/cv/index.action" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">create a CV</span></a>&nbsp;first and employers will review your CV online.</p><p style="padding: 0px; margin-bottom: 0px;">Click&nbsp;<a href="http://www.camhr.com/pages/articles/articleList.jsp?articleType=240" style="color: rgb(63, 84, 137);"><span style="color: rgb(255, 0, 0); text-decoration-line: underline;">here</span>&nbsp;</a>to learn how to register and post a CV online!</p></td></tr></tbody></table><h3 class="main-job-title-h3-2 main-job-w" style="padding: 0px; margin-right: auto; margin-bottom: 0px; margin-left: auto; font-size: 14px; width: 615px; background: rgb(238, 238, 238); line-height: 30px; height: 30px; text-indent: 10px; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif;">Contact Information</h3><table width="100%" border="0" class="main-job-w main-job-tab" style="width: 615px; margin: 0px auto; color: rgb(51, 51, 51); font-family: Arial, Helvetica, Tahoma, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);"><tbody><tr><th style="margin: 0px; line-height: 25px; width: 128px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Contact Person</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">Linda Soy (Ms.)</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Phone</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">070-824-247 / 093-300-414 / 010-999-518</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Email</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">recruitment@isisteel.com.kh</td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Website</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;"><a target="_blank" href="http://www.isisteel.com.kh/" style="color: rgb(63, 84, 137);">http://www.isisteel.com.kh</a></td></tr><tr><th style="margin: 0px; line-height: 25px; width: 125px; vertical-align: top;"><p style="padding: 0px; margin-bottom: 0px; line-height: 35px;">Address</p></th><td style="padding-top: 5px; padding-bottom: 5px; margin: 0px; line-height: 20px;">F14, KMH Industrial Park, Phum Trapang Thleung, Sangkat Chom Chao, Khan Porsenchey, Phnom Penh, Cambodia</td></tr></tbody></table>');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `Student_id` int(10) NOT NULL,
  `KhmerName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `LatinName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Pwd` varchar(255) NOT NULL,
  `Dob` date NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `SelectedCourse` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `pay_price` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`Student_id`, `KhmerName`, `LatinName`, `Email`, `Phone`, `Pwd`, `Dob`, `Gender`, `SelectedCourse`, `status`, `pay_price`) VALUES
(4, 'តាំង អ៊ុយហេង', 'Taing Uyheng', 'uyheng18@gmail.com', '086 987 846', '123', '2017-09-30', 'M', '8', 1, 100),
(5, 'dasd', 'dsad', 'Senghak@gmail.com', 'dasd', '', '2017-10-17', 'M', '8', 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `target_file` varchar(255) DEFAULT NULL,
  `file_upload` varchar(255) NOT NULL,
  `upload_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`id`, `filename`, `target_file`, `file_upload`, `upload_date`) VALUES
(12, 'Ios lesson 1', '7', 'KhmerKeyboard_NiDA_V1_01.pdf', '2017-09-30 04:35:21'),
(13, 'Java lesson 1', '9', 'KhmerKeyboard_NiDA_V1_02.pdf', '2017-09-30 04:35:39'),
(14, 'Android lesson 1', '8', 'KhmerKeyboard_NiDA_V1_03.pdf', '2017-09-30 04:44:56'),
(15, 'Web lesson 1', '10', 'KhmerKeyboard_NiDA_V1_04.pdf', '2017-09-30 04:45:10'),
(16, 'Ios lesson 2', '7', 'Phsar_Leu_Bakery_Requirement.docx', '2017-09-30 08:48:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `status` varchar(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `email`, `status`) VALUES
(1, 'admin', 'admin', 'admin@example.com', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addingcourse`
--
ALTER TABLE `addingcourse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `db_job`
--
ALTER TABLE `db_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`Student_id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addingcourse`
--
ALTER TABLE `addingcourse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `db_job`
--
ALTER TABLE `db_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `Student_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
