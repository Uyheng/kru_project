<?php
class Resource extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->model('Resource_Model');
        $this->load->library('pagination');
        $this->load->helper('url_helper');
        $this->load->helper('url');
        $this->load->model('Adding_Course');
    }
    public function view($id = ''){
        $page='student-resources';
        if(!file_exists(APPPATH.'views/Front/'.$page.'.php')){
            show_404();
        }
        if(empty($id)) {
            $pagenum = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $this->configPagination($this->Resource_Model->record_count(), 2,base_url("index.php/resource/view"));
            $data['resources'] = $this->Adding_Course->fetch_data(2, $pagenum);
            $data['cou'] = $this->Adding_Course->get_courses();

            $this->load->view('Front/header');
            $this->load->view('Front/'.$page,$data);
            $this->load->view('Front/footer');
        }else{
            $data['resources'] = $this->Adding_Course->get_where_contents($id);
            $data['cou'] = $this->Adding_Course->get_courses();

            $this->load->view('Front/header');
            $this->load->view('Front/'.$page,$data);
            $this->load->view('Front/footer');
        }
    }
    public function configPagination($totalrow, $per_page, $url) {
        $config["uri_segment"] = 3;
        $config['base_url'] = $url;
        $config['total_rows'] = $totalrow;
        $config['per_page'] = $per_page;
        $config['full_tag_open'] = "<div class='wm-pagination'><ul>";
        $config['full_tag_close'] = "</ul></div>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</a></li>";
        
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        
        $config['prev_link'] ='&larr; Previous';
        $config['prev_tag_open'] = "<li> ";
        $config['prev_tagl_close'] = "</li>";
        
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $this->pagination->initialize($config);
    }

}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/16/2017
 * Time: 10:41 PM
 */