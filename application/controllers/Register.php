<?php
class Register extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Adding_Course');
    }
    public function view($page='Register'){
        if(!file_exists(APPPATH.'views/Front/'.$page.'.php')){
            show_404();
        }
        $data['cou']=$this->Adding_Course->get_courses();
        $this->load->view('Front/header');
        $this->load->view('Front/'.$page,$data);
        $this->load->view('Front/footer');
    }
    public function create(){
    $data['KhmerName']=$this->input->post("fullname");
    $data['LatinName']=$this->input->post("latinname");
        $data['Email']=$this->input->post("email");
        $data['Phone']=$this->input->post("phone");
        $data['Pwd']=$this->input->post("comfirmpwd");
        $data['Dob']=$this->input->post("birthdate");
        $data['Gender']=$this->input->post("rdgender");
        $data['SelectedCourse']=$this->input->post("rdcourse");
        $result=$this->db->insert("registration",$data);
        if ($result) {
            $this->session->set_flashdata('flash_message', 'Registration Success!');
            redirect(base_url() . "index.php/Register/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Registration Failed!');
            redirect(base_url() . "index.php/Register/view", "refresh");
        }
    }
}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/16/2017
 * Time: 10:41 PM
 */