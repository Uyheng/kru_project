<?php
class Home extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->model('Job_Model');
        $this->load->helper('url_helper');
        $this->load->helper('url');
    }
    public function index(){

        $data['jobs'] = $this->Job_Model->get_contents();
        $this->load->view('Front/header');
        $this->load->view('Front/index',$data);
        $this->load->view('Front/footer');
    }

    public function show($id){

        $data['jobs_item'] = $this->Job_Model->get_contents($id);
        if (empty($data['jobs_item']))
        {
            show_404();
        }
        $this->load->view('Front/header');
        $this->load->view('Front/JobDetails',$data);
        $this->load->view('Front/footer');
    }
}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/15/2017
 * Time: 10:19 PM
 */