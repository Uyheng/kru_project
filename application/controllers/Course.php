<?php
class Course extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Adding_Course');
        $this->load->model('Course_Model');
        $this->load->helper('url_helper');
    }
    public function view($page='courses-list'){
        if(!file_exists(APPPATH.'views/Front/'.$page.'.php')){
            show_404();
        }
        $data['courses'] = $this->Adding_Course->get_courses_name();
        $this->load->view('Front/header');
        $this->load->view('Front/'.$page,$data);
        $this->load->view('Front/footer');
    }

    public function show($id){

        $data['course_details'] = $this->Adding_Course->get_contents_id($id);
        $this->load->view('Front/header');
        $this->load->view('Front/course-details',$data);
        $this->load->view('Front/footer');
    }
}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/16/2017
 * Time: 10:41 PM
 */