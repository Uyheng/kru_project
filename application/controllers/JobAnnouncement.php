<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class JobAnnouncement extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->model('Job_Model');
        $this->load->helper('url_helper');
        $this->load->helper('url');
    }
    public function view($page='JobAnnouncement'){
        if(!file_exists(APPPATH.'views/Front/'.$page.'.php')){
            show_404();
        }
        $data['jobs'] = $this->Job_Model->get_contents();

        $this->load->view('Front/header');
        $this->load->view('Front/'.$page,$data);
        $this->load->view('Front/footer');
    }

    public function show($id){

        $data['jobs_item'] = $this->Job_Model->get_contents($id);
        if (empty($data['jobs_item']))
        {
            show_404();
        }
        $this->load->view('Front/header');
        $this->load->view('Front/JobDetails',$data);
        $this->load->view('Front/footer');
    }

}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/16/2017
 * Time: 10:41 PM
 */