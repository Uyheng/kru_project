<?php
class Index extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Adding_Course');
        $this->load->model('Registration_Model');
    }
    public function payamount($stu_id,$payamount){
        $rm=$this->Registration_Model->getById($stu_id);
        $cp=$this->Adding_Course->getById($rm[0]->SelectedCourse);
        $data["pay_price"]=$rm[0]->pay_price+$payamount;
         if($data["pay_price"]>=$cp[0]->price){
        $data["status"]=1;
    }else{
        $data["status"]=2;
    }
    $result=$this->Registration_Model->update($stu_id,$data);
        if($result){
            $list=$this->Registration_Model->getResultWithCourse();
            $count=1;
            foreach($list as $row){
            echo   '<tr>
                              <td class="center">
                                            <div class="action-buttons">
                                                '.$count++.'
                                            </div>
                                        </td>

                                        <td><span class="label label-md label-info">'.$row->KhmerName.'</span>

                                        </td>
                                        <td>
                                            <span class="label label-md label-warning">'.$row->LatinName.'</span></td>
                                        <td>'.$row->Email.'</td>
                                        <td>'.$row->Phone.'</td>
                                        <td>'.$row->Pwd.'</td>
                                        <td class="hidden-480">'.$row->Dob.'
                                        </td>
                                        <td class="hidden-480">'.$row->Gender.'</td>
                                        <td class="hidden-480">'.$row->course_title.'</td>
                                        <td class="hidden-480">
                                        <span class="label label-md label-success">';
             if($row->status==0){echo "Pending";}else if($row->status==1){echo "Ready";}else{ echo "ខ្វះ ".($row->cprice-$row->pay_price);}
echo '</span>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs btn-group">

                                                <a onclick="doPayAll('.$row->Student_id.')"> <button class="btn btn-xs btn-success">
                                                        <i class="ace-icon fa fa-check-circle bigger-120"></i>
                                                    </button></a>

                                                <a href="#modal-table" role="button" data-toggle="modal"><button class="btn btn-xs btn-info">
                                                        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                    </button></a>


                                            </div>

                                        </td>
                                    </tr>';
            }
        }else{
            echo "error";
        }
        
    }
    public function payall($stu_id){
        $rm=$this->Registration_Model->getById($stu_id);
        $cp=$this->Adding_Course->getById($rm[0]->SelectedCourse);
        $data["status"]=1;
        $data["pay_price"]=$cp[0]->price;
        $result=$this->Registration_Model->update($stu_id,$data);
        if($result){
            $list=$this->Registration_Model->getResultWithCourse();
            $count=1;
            foreach($list as $row){
            echo   '<tr>
                              <td class="center">
                                            <div class="action-buttons">
                                                '.$count++.'
                                            </div>
                                        </td>

                                        <td><span class="label label-md label-info">'.$row->KhmerName.'</span>

                                        </td>
                                        <td>
                                            <span class="label label-md label-warning">'.$row->LatinName.'</span></td>
                                        <td>'.$row->Email.'</td>
                                        <td>'.$row->Phone.'</td>
                                        <td>'.$row->Pwd.'</td>
                                        <td class="hidden-480">'.$row->Dob.'
                                        </td>
                                        <td class="hidden-480">'.$row->Gender.'</td>
                                        <td class="hidden-480">'.$row->course_title.'</td>
                                        <td class="hidden-480">
                                        <span class="label label-md label-success">';
             if($row->status==0){echo "Pending";}else if($row->status==1){echo "Ready";}else{ echo "ខ្វះ ".($row->cprice-$row->pay_price);}
echo '</span>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs btn-group">

                                                <a onclick="doPayAll('.$row->Student_id.')"> <button class="btn btn-xs btn-success">
                                                        <i class="ace-icon fa fa-check-circle bigger-120"></i>
                                                    </button></a>

                                                <a href="#modal-table" role="button" data-toggle="modal"><button class="btn btn-xs btn-info">
                                                        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                    </button></a>


                                            </div>

                                        </td>
                                    </tr>';
            }
        }else{
            echo "error";
        }
        
    }
    public function view($page='index'){
        if($this->session->userdata('admin_login') != 1){
            redirect(base_url().'index.php/Backend/login','refresh');
        }
        if($this->session->userdata('admin_login') == 1){

            $page_data["registrations"]=$this->Registration_Model->getResultWithCourse();
            //$this->load->view('Backend/head');
            //$this->load->view('Backend/navigation');
            $this->load->view('Backend/'.$page,$page_data);
            //$this->load->view('Backend/footer');
        }
        if(!file_exists(APPPATH.'views/Backend/'.$page.'.php')){
            show_404();
        }
    }
}