<?php
class UploadJob extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
    }
    public function view($page='Upload-Job'){
        if(!file_exists(APPPATH.'views/Backend/'.$page.'.php')){
            show_404();
        }
        $this->db->order_by("id","desc");
        $page_data["jobs"]=$this->db->get("db_job")->result();
        $this->load->helper('url');
        $this->load->view('Backend/'.$page,$page_data);
    }
    public function create(){
        
        $data['jobtitle']= $this->input->post("jobtitle");
        $data['titledescription']= $this->input->post("titledescription");
        $data['companyname']= $this->input->post("companyname");
        $data['location']= $this->input->post("location");
        $data['closingdate']= $this->input->post("closingdate");
        $data['content']= $this->input->post("content");
        $result=$this->db->insert("db_job",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Upload Success!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Upload Failed!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        }
    }
    public function update($id){
        $data['jobtitle']= $this->input->post("jobtitle");
        $data['titledescription']= $this->input->post("titledescription");
        $data['companyname']= $this->input->post("companyname");
        $data['location']= $this->input->post("location");
        $data['closingdate']= $this->input->post("closingdate");
        $data['content']= $this->input->post("content");
        $this->db->where("id",$id);
        $result=$this->db->update("db_job",$data);
        if ($result) {
            $this->session->set_flashdata('flash_message', 'update Success!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'update Failed!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        } 
    }
    public function edit($id){
        $this->db->order_by("id","desc");
        $page_data["jobs"]=$this->db->get("db_job")->result();
        $page_data["edit_job"]=$this->db->get_where("db_job",array("id"=>$id))->result();
        $this->load->helper('url');
        $this->load->view('Backend/Upload-Job',$page_data);
    }
     public function delete($id){
        $this->db->where("id",$id);
        $result=$this->db->delete("db_job");
        if ($result) {
            $this->session->set_flashdata('flash_message', 'Delete Success!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Delete Failed!');
            redirect(base_url() . "index.php/Backend/UploadJob/view", "refresh");
        } 
    }
}