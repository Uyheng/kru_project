<?php
class UploadCourse extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Adding_Course');
    }
    public function view(){
        //$page='Upload-course';
        if(!file_exists(APPPATH.'views/Backend/Upload-course.php')){
            show_404();
        }
        $this->db->order_by("id","desc");
        $page_data['cou']=$this->Adding_Course->get_courses();
        $page_data["courses"]=$this->Adding_Course->get_courses_name();
        //$this->load->view('Backend/head');
        //$this->load->view('Backend/navigation');

        $this->load->view('Backend/Upload-course.php',$page_data);
        //$this->load->view('Backend/footer');
    }
    public function create(){
        $data['title']= $this->input->post("title");
        $data['course_price']= $this->input->post("course_price");
        $data['study_date']= $this->input->post("study_date");
        $data['study_time']= $this->input->post("study_time");
        $data['start_date']= $this->input->post("start_date");
        $data['duration']=$this->input->post("duration");
        $data['content']=$this->input->post("content");
        $result=$this->db->insert("course",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Upload Success!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Upload Failed!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        }
    }
    public function update($id){
        $data['title']= $this->input->post("title");
        $data['course_price']= $this->input->post("course_price");
        $data['study_date']= $this->input->post("study_date");
        $data['study_time']= $this->input->post("study_time");
        $data['start_date']= $this->input->post("start_date");
        $data['duration']=$this->input->post("duration");
        $data['content']=$this->input->post("content");
        $this->db->where("id",$id);
        $result=$this->db->update("course",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Update Success!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Update Failed!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        } 
       
    }

    public function edit($id){
        $this->db->order_by("id","desc");
        $page_data["courses"]=$this->Adding_Course->get_courses_name();
        $page_data['cou'] = $this->Adding_Course->get_courses();
        $page_data["edit_course"]=$this->db->get_where("course",array("id"=>$id))->result();
        $this->load->helper('url');
        // $this->load->view('Backend/head');
        // $this->load->view('Backend/navigation');
        $this->load->view('Backend/Upload-course',$page_data);
    }
    public function delete($id){
        $this->db->where("id",$id);
        $result=$this->db->delete("course");
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Delete Success!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Delete Failed!');
            redirect(base_url() . "index.php/Backend/UploadCourse/view", "refresh");
        }
    }
    
   
}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/15/2017
 * Time: 10:19 PM
 */