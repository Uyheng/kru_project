<?php
class UploadResource extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Adding_Course');

    }
    
    public function view($page='Upload-resource'){
        if(!file_exists(APPPATH.'views/Backend/'.$page.'.php')){
            show_404();
        }

        $this->db->order_by("id","desc");
        $page_data["resources"]=$this->Adding_Course->get_target_name();
        $page_data['cou']=$this->Adding_Course->get_courses();
        $this->load->helper('url');
        // $this->load->view('Backend/head');
        // $this->load->view('Backend/navigation');
        $this->load->view('Backend/'.$page,$page_data);
        //  $this->load->view('Backend/footer');
    }
    public function create(){
                     $config['upload_path'] = './uploads/resource/';
                     $config['allowed_types'] = 'gif|jpg|png|jpeg|pptx|pdf|doc|docx|jpeg|zip|rar';
                     $config['max_size'] =20000;
                     $config['max_width'] = 1111024;
                     $config['max_height'] = 111768;

                     $this->load->library('upload', $config);
                    if ($_FILES['file_upload']['size'] !=0){
                        if (!$this->upload->do_upload('file_upload')) {
                             $error = array('error' => $this->upload->display_errors());
                             $this->session->set_flashdata('error_message', $error);
                             var_dump($error);
                             return;
                             redirect(base_url()."index.php/Backend/UploadResource/view","refresh"); 
                         } else {
                            $data1 = array('upload_data' => $this->upload->data());
                            $data["file_upload"]=$data1["upload_data"]["file_name"];
                        }
                    }
        $data['filename']= $this->input->post("filename");
        $data['target_file']= $this->input->post("target_file");
        $data['upload_date']=date('Y-m-d H:i:s');
        $result=$this->db->insert("resource",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Upload Success!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Upload Failed!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        }
    }
    public function update($id){
         $config['upload_path'] = './uploads/resource/';
                     $config['allowed_types'] = 'gif|jpg|png|jpeg|pptx|pdf|doc|docx|jpeg|zip|rar';
                     $config['max_size'] =20000;
                     $config['max_width'] = 1111024;
                     $config['max_height'] = 111768;

                     $this->load->library('upload', $config);
                    if ($_FILES['file_upload']['size'] !=0){
                        if (!$this->upload->do_upload('file_upload')) {
                             $error = array('error' => $this->upload->display_errors());
                             $this->session->set_flashdata('error_message', $error);
                             var_dump($error);
                             return;
                             redirect(base_url()."index.php/Backend/UploadResource/view","refresh"); 
                         } else {
                            $data1 = array('upload_data' => $this->upload->data());
                            $data["file_upload"]=$data1["upload_data"]["file_name"];
                        }
                    }
        $data['filename']= $this->input->post("filename");
        $data['target_file']= $this->input->post("target_file");
        $data['upload_date']=date('Y-m-d H:i:s');
        $this->db->where("id",$id);
        $result=$this->db->update("resource",$data);
        if ($result) {
            $this->session->set_flashdata('flash_message', 'Update Success!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Update Failed!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        }
    }

    public function edit($id){
        $this->db->order_by("id","desc");
        $page_data["resources"]=$this->Adding_Course->get_target_name();
        $page_data["edit_res"]=$this->db->get_where("resource",array("id"=>$id))->result();
        $page_data['cou']=$this->Adding_Course->get_courses();
        $this->load->helper('url');
        // $this->load->view('Backend/head');
        // $this->load->view('Backend/navigation');
        $this->load->view('Backend/Upload-resource',$page_data);
    }
    public function delete($id){
        $this->db->where("id",$id);
        $result=$this->db->delete("resource");
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Delete Success!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Delete Failed!');
            redirect(base_url() . "index.php/Backend/UploadResource/view", "refresh");
        }
    }

}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/15/2017
 * Time: 10:19 PM
 */