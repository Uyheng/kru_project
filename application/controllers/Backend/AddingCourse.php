<?php
class AddingCourse extends CI_Controller{

    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
    }
    public function view($page='Adding-course'){
        if(!file_exists(APPPATH.'views/Backend/'.$page.'.php')){
            show_404();
        }
        $this->db->order_by("id","desc");
        $page_data["addingcourse"]=$this->db->get("addingcourse")->result();
        $this->load->helper('url');
        //$this->load->view('Backend/head');
        //$this->load->view('Backend/navigation');
        $this->load->view('Backend/'.$page,$page_data);
        //$this->load->view('Backend/footer');
    }
    public function create(){
        $data['title']= $this->input->post("title");
        $data['title_description']= $this->input->post("title_description");
         $result=$this->db->insert("addingcourse",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Upload Success!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Upload Failed!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        }
    }
    public function update($id){
        $data['title']= $this->input->post("title");
        $data['title_description']= $this->input->post("title_description");
        $this->db->where("id",$id);
        $result=$this->db->update("addingcourse",$data);
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Update Success!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Update Failed!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        }

    }

    public function edit($id){
        $this->db->order_by("id","desc");
        $page_data["addingcourse"]=$this->db->get("addingcourse")->result();
        $page_data["edit_course"]=$this->db->get_where("addingcourse",array("id"=>$id))->result();
        $this->load->helper('url');
        // $this->load->view('Backend/head');
        // $this->load->view('Backend/navigation');
        $this->load->view('Backend/Adding-course',$page_data);
    }
    public function delete($id){
        $this->db->where("id",$id);
        $result=$this->db->delete("addingcourse");
        if ($result) {

            $this->session->set_flashdata('flash_message', 'Delete Success!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        } else {
            $this->session->set_flashdata('error_message', 'Delete Failed!');
            redirect(base_url() . "index.php/Backend/AddingCourse/view", "refresh");
        }
    }


}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/15/2017
 * Time: 10:19 PM
 */