<?php
class Aboutus extends CI_Controller{

    function  __construct() {
        parent::__construct();
    }
    public function view($page='Aboutus'){
        if(!file_exists(APPPATH.'views/Front/'.$page.'.php')){
            show_404();
        }
        $this->load->helper('url');
        $this->load->view('Front/header');
        $this->load->view('Front/'.$page);
        $this->load->view('Front/footer');
    }

}
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/16/2017
 * Time: 10:41 PM
 */