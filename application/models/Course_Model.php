<?php
class Course_Model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    function get_contents($id=false)
    {
        if ($id === FALSE) {
            $query = $this->db->get('course');
            return $query->result_array();
        }

        $query = $this->db->get_where('course', array('id' => $id));
        return $query->row_array();
    }
}