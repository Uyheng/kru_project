<?php
class Adding_Course extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    public function getById($id){
        $this->db->where("id",$id);
         return $this->db->get("addingcourse")->result();
    }
     public function fetch_data($limit, $start) {
        $this->db->select('a.*,b.*');
        $this->db->from('addingcourse a');
        $this->db->join('resource b','b.target_file=a.id','inner');
        $this->db->limit($limit, $start);
        return  $this->db->get()->result();
    }
    function get_courses()
    {
        $query=$this->db->get('addingcourse');
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }
    function get_courses_name(){
        $this->db->select('a.*,b.title as course_title');
        $this->db->from('course a');
        $this->db->join('addingcourse b','b.id=a.title','inner');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }
    function get_target_name(){
        $this->db->select('a.*,b.title as course_title');
        $this->db->from('resource a');
        $this->db->join('addingcourse b','b.id=a.target_file','inner');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }
    function get_register_course(){
        $this->db->select('a.*,b.title as course_title');
        $this->db->from('registration a');
        $this->db->join('addingcourse b','b.id=a.SelectedCourse','inner');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }
    function get_contents_id($id='')
    {
        $this->db->select('a.*,b.title as course_title');
        $this->db->from('course a');
        $this->db->join('addingcourse b','b.id=a.title','inner');
        $this->db->where('a.id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }


    function get_contents()
    {
        $this->db->select('a.*,b.*');
        $this->db->from('addingcourse a');
        $this->db->join('resource b','b.target_file=a.id','inner');
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }

    function get_where_contents($id = ''){
        $this->db->select('a.*,b.*');
        $this->db->from('addingcourse a');
        $this->db->join('resource b','b.target_file=a.id','inner');
        $this->db->where('a.id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        }
        return false;
    }
}