<?php
class Job_Model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    function get_contents($id=false){
        if ($id === FALSE)
        {
            $query = $this->db->get('db_job');
            return $query->result_array();
        }

        $query = $this->db->get_where('db_job', array('id' => $id));
        return $query->row_array();
    }
}