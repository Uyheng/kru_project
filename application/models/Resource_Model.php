<?php
class Resource_Model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    public function record_count() {
        $result= $this->db->get("addingcourse")->result();
       return count($result);
    }
    function get_contents($id=false){
        if ($id === FALSE)
        {
            $query = $this->db->get('resource');
            return $query->result_array();
        }

        $query = $this->db->get_where('resource', array('id' => $id));
        return $query->row_array();
    }
}