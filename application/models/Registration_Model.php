<?php
class Registration_Model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
    }
    public function update($id,$data){
        $this->db->where("Student_id",$id);
        $result=$this->db->update("registration",$data);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    public function getResultWithCourse(){
        $this->db->select("a.*,b.price as cprice,b.price as course_title");
        $this->db->from("registration a");
        $this->db->join("addingcourse b","a.SelectedCourse=b.id");
        return $this->db->get()->result();
    }

    public function getById($id){
        return $this->db->get_where("registration",array("Student_id"=>$id))->result();
    }
    function get_contents($id=false){
        if ($id === FALSE)
        {
            $query = $this->db->get('registration');
            return $query->result_array();
        }

        $query = $this->db->get_where('registration', array('id' => $id));
        return $query->row_array();
    }
}