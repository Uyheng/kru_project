
<!--// Footer \\-->
<footer id="wm-footer" class="wm-footer-one">
    <!--// Footercompany \\-->
    <div class="wm-footer-newslatter">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form>
                        <i><a href="<?php echo base_url('index.php/home/index')?>" class="wm-footer-logo"><img src="<?php echo base_url();?>assets/images/logo-4.png" alt="" style="width:200px; height:100px;margin-right:50px;border-radius: 25px;float:left;"></a></i>
                        <ul style="color:white;list-style-type:none;float:left;">
                            <li>UYHENG EDUCATiON GROUP <br> (Uyheng-CENTER)</li>
                            <li>#55, Street 1BD-184, Sangkat Boeung Raing,Khan Daun Penh,Phnom Penh,Cambodia</li>
                            <li>Tel: +855(023 6310 310)</li>
                            <li>Email: Info@Uyheng-Center.org</li>
                        </ul>
                        <ul style="color:white;list-style-type:none;float:right;">
                            <li>Follow us on</li>
                            <li><div class="wm-footer-icons">
                                    <a href="#" class="wmicon-social5"></a>
                                    <a href="#" class="wmicon-social7"></a>
                                    <a href="#" class="wmicon-social4"></a>
                                    <a href="#" class="wmicon-social3"></a>
                                    <a href="#" class="wmicon-vimeo"></a>
                                </div></li>
                            <li>CD-Center@2017 | All Rights Reserved. <br> Theme by GGWP</li>
                        </ul>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--// Footercompany \\-->

    <!--// FooterCopyRight \\-->
    <div class="wm-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6"> <span><i class="wmicon-nature"></i> Phnom Penh, Cambodia 28°F / 29°C</span> </div>
                <div class="col-md-6"> <p>© 2017, All Right Reserved - by <a href="index.php" class="wm-color">newbie</a></p> </div>
            </div>
        </div>
    </div>
    <!--// FooterCopyRight \\-->

</footer>
<!--// Footer \\-->

<div class="clearfix"></div>
</div>
<!--// Main Wrapper \\-->


<!-- jQuery (necessary for JavaScript plugins) -->
<script type="text/javascript" src="<?php echo base_url();?>assets/script/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/jquery.prettyphoto.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/fitvideo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/skills.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/slick.slider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/waypoints-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/build/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/isotope.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/script/functions.js"></script>

</body>
</html>