
		<!--// Main Content \\-->
		<div class="wm-main-content">

			<!--// Main Section \\-->
			<div class="wm-main-section">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="wm-classic-heading">
								<h2>Job Announcement</h2>
							</div>
						</div>
						<div class="col-md-12">
                            <table class="table table-inverse">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Job Title</th>
                                    <th>Company Name</th>
                                    <th>Location</th>
                                    <th>Closing Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($jobs as $jobs_item):?>

                                <tr>
                                    <th scope="row">1</th>
                                    <td><?php echo $jobs_item['jobtitle']; ?></td>
                                    <td><?php echo $jobs_item['companyname']; ?></td>
                                    <td><?php echo $jobs_item['location']; ?></td>
                                    <td><?php echo $jobs_item['closingdate']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url('index.php/JobAnnouncement/show/'.$jobs_item['id'])?>"><button class="btn btn-info btn-sm">More Details</button></a>
                                    </td>
                                </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>

						</div>


					</div>
				</div>
			</div>
			<!--// Main Section \\-->

		</div>
		<!--// Main Content \\-->
