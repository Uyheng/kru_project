<head>
    <style>
        body {
            background-color: #eee;
        }

        *[role="form"] {
            max-width: 530px;
            padding: 15px;
            margin: 0 auto;
            background-color: #fff;
            border-radius: 0.3em;
        }

        *[role="form"] h2 {
            margin-left: 5em;
            margin-bottom: 1em;
        }


    </style>
</head>
<div class="container">
    <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/Register/create/");?>" id="form-job" method="post" enctype="multipart/form-data">
        <h2 class="text-left">ចុះឈ្មោះចូលរៀន</h2>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">ឈ្មោះពេញ</label>
            <div class="col-sm-9">
                <input type="text" id="fullname" name="fullname" placeholder=" តាំង អ៊ុយហេង" class="form-control" autofocus>

            </div>
        </div>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">អក្សរឡាតាំង</label>
            <div class="col-sm-9">
                <input type="text" id="latinname" name="latinname" placeholder="Taing Uyheng" class="form-control" autofocus>

            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">អ៊ីម៉ែល</label>
            <div class="col-sm-9">
                <input type="email" id="email" name="email" placeholder="uyheng18@gmail.com" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">លេខទូរស័ព្ទ</label>
            <div class="col-sm-9">
                <input type="phone" id="phone" name="phone" placeholder="012 889 123" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">លេខសំងាត់</label>
            <div class="col-sm-9">
                <input type="password" id="password" name="password" placeholder="Password" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">បញ្ជាក់លេខសំងាត់</label>
            <div class="col-sm-9">
                <input type="password" id="comfirmpwd" name="comfirmpwd" placeholder="Password" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="birthDate" class="col-sm-3 control-label">ថ្ងៃខែឆ្នាំកំណើត</label>
            <div class="col-sm-9">
                <input type="date" id="birthdate" name="birthdate" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">ភេទ</label>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-9">
                        <label class="radio-inline">
                            <input type="radio" id="rdgender" name="rdgender" value="M">ប្រុស
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="rdgender" name="rdgender" value="F">ស្រី
                        </label>
                    </div>
                </div>
            </div>
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label class="control-label col-sm-3">ចង់រៀន</label>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-9">
                        <?php if(is_array($cou)){foreach ($cou as $row):?>
                            <label class="radio-inline">
                                <input type="radio" id="rdcourse" name="rdcourse" value="<?php echo $row->id;?>" ><?php echo $row->title; ?>
                            </label>
                        <?php endforeach;}?>
                    </div>
                </div>
            </div>
        </div> <!-- /.form-group -->
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
        </div>
    </form> <!-- /form -->
</div> <!-- ./container -->