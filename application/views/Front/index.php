
		<!--// Main Content \\-->
		<div class="wm-main-content">

			<!--// Main Section \\-->
			<div class="wm-main-section">
				<div class="container">
					<div class="row">
						
						<div class="col-md-4">
							<div class="wm-search-course">
                                <h3 class="wm-short-title wm-color">Find Your Course</h3>
                                <p>Here are the courses that we provide</p>

                            </div>
						</div>
                        <div class="col-md-8">
                            <div class="wm-service wm-box-service">
                                <ul>
                                    <li>
                                        <div class="wm-box-service-wrap wm-bgcolor">
                                            <i class="wmicon-web3"></i>
                                            <h6><a href="<?php echo base_url('index.php/course/view')?>">Android</a></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="wm-box-service-wrap wm-bgcolor">
                                            <i class="wmicon-computer"></i>
                                            <h6><a href="<?php echo base_url('index.php/course/view')?>">IOS</a></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="wm-box-service-wrap wm-bgcolor">
                                            <i class="wmicon-technology3"></i>
                                            <h6><a href="<?php echo base_url('index.php/course/view')?>">Web Dev</a></h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="wm-box-service-wrap wm-bgcolor">
                                            <i class="wmicon-web3"></i>
                                            <h6><a href="<?php echo base_url('index.php/course/view')?>">JAVA</a></h6>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

					</div>
				</div>
			</div>
			<!--// Main Section \\-->


            <!--// Main Section \\-->
            <div class="wm-main-section wm-news-grid-full">
                <div class="container">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="wm-fancy-title"> <h2>Job <span>Announcement</span></h2> <p>We bring you all the useful information of the Job Market in IT related fields</p> </div>
                            <div class="wm-news wm-news-grid">
                                <ul class="row">
                                    <?php foreach ($jobs as $jobs_item):?>
                                    <li class="col-md-4">
                                        <figure>
                                            <a href="#"><img src="extra-images/news-grid-1.png" alt=""></a>
                                            <figcaption class="wm-bgcolor">
                                                <img src="extra-images/news-grid-thumb-1.png" alt="">
                                                <h6>Hired by: <a href="#"><?php echo $jobs_item['companyname']; ?></a></h6>
                                            </figcaption>
                                        </figure>
                                        <div class="wm-newsgrid-text">

                                            <h5><a href="#" class="wm-color"><?php echo $jobs_item['jobtitle']; ?></a></h5>
                                            <p><?php echo $jobs_item['titledescription']; ?></p>
                                            <p><?php echo 'Location: '.$jobs_item['location']; ?></p>
                                            <p><?php echo 'Closing Date: '.$jobs_item['closingdate']; ?></p>
                                            <a class="wm-banner-btn" href="<?php echo base_url('index.php/home/show/'.$jobs_item['id'])?>">I Want to know more...</a>
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--// Main Section \\-->

		<!--// Main Content \\-->
