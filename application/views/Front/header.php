<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Education Home Page1</title>

    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/flaticon.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/slick-slider.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/prettyphoto.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/build/mediaelementplayer.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/color.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/color-two.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/color-three.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/color-four.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!--// Main Wrapper \\-->
    <div class="wm-main-wrapper">

        <!--// Header \\-->
		<header id="wm-header" class="wm-header-one">

            <!--// TopStrip \\-->
			<div class="wm-topstrip">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wm-language"> <ul> <li><a href="#">English</a></li> <li><a href="#">ខ្មែរ</a></li> </ul> </div>
                            <ul class="wm-stripinfo">
                                <li><i class="wmicon-location"></i> #430,271st, Sangkat Terk Tla, Khan Sen Sok, Phnom Penh</li>
                                <li><i class="wmicon-technology4"></i>086 987 846</li>
                                <li><i class="wmicon-clock2"></i> Mon - fri: 9:00am - 5:00pm</li>
                            </ul>
                            <ul class="wm-adminuser-section">
                                <li><a href="<?php echo base_url('index.php/register/view')?>">Register</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!--// TopStrip \\-->

            <!--// MainHeader \\-->
            <div class="wm-main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3"><a href="<?php echo base_url('index.php/home/index')?>" class="wm-logo"><img width="200px" height="100px" src="<?php echo base_url();?>assets/images/logo-4.png" alt=""></a></div>
                        <div class="col-md-9">
                            <!--// Navigation \\-->
                            <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="true">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>
                                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                  <ul class="nav navbar-nav">
                                    <li class="active"><a href="<?php echo base_url('index.php/home/index')?>">Home</a>

                                    </li>
                                    <li><a href="<?php echo base_url('index.php/course/view')?>">Courses</a>

                                    </li>
                                    <li><a href="<?php echo base_url('index.php/resource/view')?>">Download Resources</a>

                                    </li>
                                    <li ><a href="<?php echo base_url('index.php/JobAnnouncement/view')?>">Job Announcement</a>

                                    </li>

                                    <li><a href="<?php echo base_url('index.php/Aboutus/view')?>">Contact Me</a>

                                    </li>
                                  </ul>
                                </div>
                            </nav>
                            <!--// Navigation \\-->
                          <!--  <a href="#" class="wm-header-btn">get started</a>-->
                        </div>
                    </div>
                </div>
            </div>
            <!--// MainHeader \\-->

		</header>
		<!--// Header \\-->

