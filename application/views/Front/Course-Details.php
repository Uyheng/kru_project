		<!--// Main Content \\-->
		<div class="wm-main-content">

            <!--// Main Section \\-->
            <div class="wm-main-section">
                <div class="container">
                    <div class="row">

                        <div class="col-md-9">
                            <!--// Editore \\-->
                            <div class="wm-detail-editore wm-custom-space">
                                <h3><?php echo $course_details[0]->course_title; ?></h3>
                                    <!-- COURSE DESCRIPTION GO HERE-->
                                <?php echo $course_details[0]->content; ?>
                                <!-- COURSE DESCRIPTION END HERE-->
                            </div>
                        </div>
                        <aside class="col-md-3">
                            <div class="wm-event-options">
                                <ul>
                                    <li>
                                        <i class="wmicon-time2"></i>
                                        <span>Start Date:</span>
                                        <p><?php echo $course_details[0]->start_date; ?></p>
                                    </li>
                                    <li>
                                        <i class="wmicon-time2"></i>
                                        <span>Study Date:</span>
                                        <p><?php echo $course_details[0]->study_date; ?></p>
                                    </li>
                                    <li>
                                        <i class="wmicon-clock2"></i>
                                        <span>Study time:</span>
                                        <p><?php echo $course_details[0]->study_time; ?></p>
                                    </li>
                                    <li>
                                        <i class="wmicon-clock2"></i>
                                        <span>Course Duration:</span>
                                        <p><?php echo $course_details[0]->duration; ?></p>
                                    </li>
                                    <li>
                                        <i class="wmicon-money"></i>
                                        <span>Price:</span>
                                        <p><?php echo $course_details[0]->course_price; ?></p>
                                    </li>

                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
            <!--// Main Section \\-->
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=413703932324856";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-comments" data-href="http://elearning.com/index.php/course/show/<?php echo $course_details[0]->id; ?>" data-numposts="5"></div>
		</div>
		<!--// Main Content \\-->
	<div class="clearfix"></div>

