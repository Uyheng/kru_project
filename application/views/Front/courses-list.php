

    <!--// Main Wrapper \\-->
    <div class="wm-main-wrapper">

		<!--// Main Content \\-->
		<div class="wm-main-content">

            <!--// Main Section \\-->
            <div class="wm-main-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="wm-courses wm-courses-popular wm-courses-mediumsec">
                                <ul class="row">
                                    <li class="col-md-12">
                                        <?php foreach ($courses as $course_items):?>
                                        <div class="wm-courses-popular-wrap">
                                            <figure>
                                                <figcaption>
                                                    <h6><a href="#"><?php echo $course_items->course_title; ?></a></h6>
                                                </figcaption>
                                                <a href="<?php echo base_url('index.php/course/show/'.$course_items->id);?>"><img src="<?php echo base_url();?>assets/images/coding.png" alt=""></a>
                                            </figure>
                                            <div class="wm-popular-courses-text">
                                                <h6><a href="<?php echo base_url('index.php/course/show/'.$course_items->id);?>"><?php echo $course_items->course_title; ?></a></h6>
                                                <p><?php echo substr($course_items->content,0,100); ?>.</p>

                                                <div class="wm-courses-price"> <span>$<?php echo $course_items->course_price; ?></span> </div>
                                                <ul>
                                                    <li><a href="#" class="wm-color"><i class="wmicon-time2"></i><?php echo $course_items->study_date; ?></a></li>
                                                    <li><a href="#" class="wm-color"><i class="wmicon-time2"></i><?php echo $course_items->study_time; ?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!--// Main Section \\-->

		</div>
		<!--// Main Content \\-->

	<div class="clearfix"></div>
    </div>

    <!--// Main Wrapper \\-->


