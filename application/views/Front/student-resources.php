		<!--// Main Content \\-->
		<div class="wm-main-content">

			<!--// Main Section \\-->
			<div class="wm-main-section">
				<div class="container">
					<div class="row">
						<aside class="col-md-4">
							<div class="wm-student-dashboard-nav">

								<div class="wm-student-nav">
                                    <h3 class="wm-short-title wm-color">Select Courses</h3>

									<ul>
                                        <?php if(is_array($cou)){foreach ($cou as $row):?>
										<li>
                                            <a id="filtercourse" href="<?php echo base_url()."index.php/resource/view/".$row->id; ?>">
                                                <i class="wmicon-book"></i>
                                                    <input type="hidden" value="<?php echo $row->id;?>" name="course_id"/>
                                                    <?php echo $row->title; ?>
                                            </a>
										</li>
                                        <?php endforeach;}?>
									</ul>
								</div>
							</div>							
						</aside>
						<div class="col-md-8">

							<div class="wm-student-dashboard-statement wm-dashboard-statement">
								<div class="wm-plane-title">
									<h2>Resource</h2>
								</div>
								<div class="wm-article">
									<ul>
										<li class="wm-statement-start wm-student">
											<span>File Name</span>
										</li>
                                        <li class="wm-student">
                                            <span>Size</span>
                                        </li>
										<li class="wm-student">
											<span>Action</span>
										</li>
									</ul>									
								</div>
                                <?php foreach ($resources as $resource_items):?>
                                <div class="wm-article">
                                    <ul>
                                        <li class="wm-statement-start">
                                            <div class="wm-statement-started-text">
                                                <h6><a href="#"><?php echo $resource_items->filename; ?></a></h6>
                                                <span><a href="#" class="wmicon-tag"></a>PDF FILE</span>
                                                <span><a href="#" class="wmicon-time2"></a><?php echo $resource_items->upload_date; ?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <small>One</small>
                                        </li>
                                        <li>
                                            <a class="wm-dowmload" href="<?php echo base_url('uploads/resource/'.$resource_items->file_upload);?>"><i class="fa fa-download"></i>Download</a>
                                        </li>
                                    </ul>
                                </div>
                                <?php endforeach; ?>
<!--                                <div class="wm-pagination">
                                    <ul>
                                        <li><a href="#" aria-label="Previous"> <i class="wmicon-arrows4"></i> Previous</a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>

                                        <li>...</li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#" aria-label="Next"> <i class="wmicon-arrows4"></i> Next</a></li>
                                    </ul>
                                </div>-->
<?php echo $this->pagination->create_links(); ?>
							</div>										
						</div>
					</div>
				</div>
			</div>
			<!--// Main Section \\-->
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=413703932324856";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-comments" data-href="http://elearning.com/index.php/resource/view" data-numposts="5"></div>
            <!--// Main Section \\-->
		</div>
		<!--// Main Content \\-->
