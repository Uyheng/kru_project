<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Form Elements - Ace Admin</title>

    <meta name="description" content="Common form elements and layouts" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link href="<?php echo base_url();?>assets/css/toastr.min.css" rel='stylesheet' type='text/css' />

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/daterangepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/bootstrap-colorpicker.min.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/ace-rtl.min.css" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/Backend-Assets/css/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="<?php echo base_url();?>assets/Backend-Assets/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="<?php echo base_url();?>assets/Backend-Assets/js/html5shiv.min.js"></script>
    <script src="<?php echo base_url();?>assets/Backend-Assets/js/respond.min.js"></script>
    <![endif]-->
    <!-- Include CSS for icons. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Create a tag that we will use as the editable area. -->
    <!-- You can use a div tag as well. -->


    <!-- Include jQuery lib. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>

    <!-- Initialize the editor. -->
</head>
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    Uy Heng Admin
                </small>
            </a>
        </div>
        <div class="navbar-header pull-right">

            <a href="<?php echo base_url('index.php/Backend/login/logout')?>" type="button" class="btn btn-danger">Log Out</a>
        </div>

    </div><!-- /.navbar-container -->
</div>