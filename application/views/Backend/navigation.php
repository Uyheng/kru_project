<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <ul class="nav nav-list">

        <li class="active">
            <a href="<?php echo base_url('index.php/Backend/index/view')?>">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> DashBoard </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li>
            <a href="<?php echo base_url('index.php/Backend/UploadJob/view')?>">
                <i class="menu-icon fa fa-briefcase"></i>
                <span class="menu-text"> Upload Job </span>
            </a>

            <b class="arrow"></b>
        </li>
        <!--Upload Resources-->
        <li>
            <a href="<?php echo base_url('index.php/Backend/UploadResource/view')?>">
                <i class="menu-icon fa fa-folder-open-o"></i>
                <span class="menu-text">  Upload Resource  </span>
            </a>

            <b class="arrow"></b>
        </li>
        
        <li>
            <a href="<?php echo base_url('index.php/Backend/UploadCourse/view')?>">
                <i class="menu-icon fa fa-html5"></i>
                <span class="menu-text">  Upload Course  </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li>
            <a href="<?php echo base_url('index.php/Backend/AddingCourse/view')?>">
                <i class="menu-icon fa fa-plus-circle"></i>
                <span class="menu-text">  Adding Course  </span>
            </a>

            <b class="arrow"></b>
        </li>
        
    </ul><!-- /.nav-list -->

</div>